//INSERTING SINGLE ROOM

//[Insert One]
db.rooms.insert({
	name: "single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})
//[Insert One End]


//INSERTING MULTIPLE ROOMS

//[Insert Many]

db.rooms.insertMany([
	{
	name: "double",
	accommodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: false
	},
	{
	name: "queen",
	accommodates: 4,
	price: 4000,
	description: "A room with queen sized bed perfect for a simple getaway",
	rooms_available: 15,
	isAvailable: false
	}
])

//[Insert End]


//Finding room with name double
db.rooms.find({ name: "double" })
//Finding room with name double End


//Updating queen rooms available to 0
db.rooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: "0"
		}
	}
)
//Updating queen rooms available to 0 End


//Delete all rooms that have 0 rooms available
db.rooms.deleteMany({
	rooms_available: "0"
})
//Delete all rooms that have 0 rooms available End